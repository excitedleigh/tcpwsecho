package main

import (
    "bufio"
    "net"
    "net/http"
    "golang.org/x/net/websocket"
    "log"
)

const html = `<html>
<head>
    <style type="text/css">
        body {
            background-color: black;
            color: white;
            font-family: monospace;
            font-size: 25px;
        }
    </style>
</head>
<body>
    <div id="msgbox">
    </div>
    <script type="text/javascript">
        var ws = new WebSocket('ws://' + document.location.host + '/ws')
        ws.onmessage = function(m){
            var msgbox = document.getElementById('msgbox')
            var mydiv = document.createElement('div')
            mydiv.textContent = m.data
            msgbox.appendChild(mydiv)
        }
    </script>
</body>
</html>
`

func fanOut(inputChan <-chan []byte, size, lag int) chan chan []byte {
    outputChanChan := make(chan chan []byte)
    go func(){
        outputChans := make([]chan []byte, size)
        for {
            select {
                case newOutputChan := <-outputChanChan:
                    outputChans = append(outputChans, newOutputChan)
                    log.Printf("Registered new fanOut listener, now %s", len(outputChans))
                case newData := <-inputChan:
                    log.Printf("Got input to fanOut %s", newData)
                    for _, c := range outputChans {
                        log.Printf("Sending %s to %s", newData, c)
                        select {
                        case c <- newData:
                            log.Print("sent message")
                        default:
                            log.Print("no message sent")
                        }
                    }
            }
        }
    }()
    return outputChanChan
}

func tcpHandle(addr string, handleConnection func(net.Conn)){
    go func(){
        log.Print("Listening on tcp")
        ln, err := net.Listen("tcp", ":1337")
        if err != nil {
            log.Printf("Listen error %s", err)
        }
        for {
            conn, err := ln.Accept()
            if err != nil {
                log.Printf("Connect error %s", err)
            }
            log.Print("Accepted connection on tcp")
            go handleConnection(conn)
        }
    }()
}

func main(){
    log.Print("Setting up")
    inputChan := make(chan []byte)
    outputChanChan := fanOut(inputChan, 5, 100)

    // set up ws server
    http.Handle("/ws", websocket.Handler(func(ws *websocket.Conn){
        myChan := make(chan []byte)
        outputChanChan <- myChan
        for v := range myChan {
            ws.Write(v)
        }
    }))

    // set up tcp server
    tcpHandle(":1337", func(conn net.Conn){
        defer conn.Close()
        reader := bufio.NewReader(conn)
        for {
            input, err := reader.ReadBytes('\n')
            if err != nil {
                log.Printf("Error reading from socket %s", err)
                return
            }
            log.Printf("Got bytes from tcp %s", input)
            inputChan <- input
        }
    })

    // serve static files
    //http.Handle("/", http.FileServer(http.Dir("webroot")))
    http.HandleFunc("/", func(rw http.ResponseWriter, req *http.Request){
        rw.Write([]byte(html))
    })

    log.Print("Listening on http")
    log.Fatal(http.ListenAndServe(":8888", nil))

}
